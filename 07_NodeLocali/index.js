const express = require('express')
const bodyParser = require('body-parser')
const pool = require('./dbConfig')
const cors = require('cors')
const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({ extender: true }))

const port = 4000
const username = "pillipillo"
const password = "provaprova1"
const dbName = "locali"

/* mongoose.connect(`mongodb+srv://${username}:${password}@cluster0.req1t.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono connesso al DB Mongo!")
}) */
app.listen(port, () => {
    console.log(`Sono in ascolto sulla porta ${port}`)
})

app.get("/locali/list", async (req, res) => {
    try {
        let elenco = await pool.query('SELECT * FROM locale');

        console.log(`Numero di righe: ${elenco.rowCount}`)

        res.json(elenco.rows)
    } catch (error) {
        console.log(error)
    }
})

app.get("/locali/:idLocale", async (req, res) => {
    try {
        let risultato = await pool.query('SELECT * FROM locale WHERE id = $1', [req.params.idLocale])

        res.json(risultato.rows)
    } catch (error) {
        console.log(error)
    }
})

app.post("/locali/insert", async (req, res) => {
    try {
        let querySql = `INSERT INTO locale (nome, descrizione, citta, provincia, regione, lat, long, voto)` +
            `VALUES ('${req.body.nome}', '${req.body.descrizione}', '${req.body.citta}', '${req.body.provincia}', '${req.body.regione}', '${req.body.lat}', '${req.body.long}', '${req.body.voto}')`;

        let risultatoInserimento = await pool.query(querySql)

        if (risultatoInserimento.rowCount > 0) {
            res.json({ status: "success" })
        }
        else {
            res.json({ status: "error" })
        }
    } catch (error) {
        console.log(error)
    }
})

app.delete("/locali/:idLocale", async (req, res) => {
    try {
        let esitoOperazione = await pool.query(`DELETE FROM locale WHERE id = ${req.params.idLocale}`)

        if (esitoOperazione.rowCount > 0) {
            res.json({ status: "eliminato con successo!" })
        }
        else {
            res.json({ status: "errore, dato non trovato" })
        }
    } catch (error) {
        console.log(error)
    }
})

app.put("/locali/:idLocale", async (req, res) => {
    try {
        let oggettoPreSalvataggio = await pool.query(`SELECT * FROM locale WHERE id = ${req.params.idLocale}`)
        personaPreSalvataggio = oggettoPreSalvataggio.rows[0]

        if (req.body.nome)
            personaPreSalvataggio.nome = req.body.nome
        if (req.body.descrizione)
            personaPreSalvataggio.descrizione = req.body.descrizione
        if (req.body.citta)
            personaPreSalvataggio.citta = req.body.citta
        if (req.body.provincia)
            personaPreSalvataggio.provincia = req.body.provincia
        if (req.body.regione)
            personaPreSalvataggio.regione = req.body.regione
        if (req.body.lat)
            personaPreSalvataggio.lat = req.body.lat
        if (req.body.long)
            personaPreSalvataggio.long = req.body.long
        if (req.body.voto)
            personaPreSalvataggio.voto = req.body.voto

        //UPDATE

        let queryUpdate = `UPDATE locale SET ` +
            `nome = '${personaPreSalvataggio.nome}', ` +
            `descrizione = '${personaPreSalvataggio.descrizione}', ` +
            `citta = '${personaPreSalvataggio.citta}', ` +
            `provincia = '${personaPreSalvataggio.provincia}', ` +
            `regione = '${personaPreSalvataggio.regione}', ` +
            `lat = '${personaPreSalvataggio.lat}', ` +
            `long = '${personaPreSalvataggio.long}', ` +
            `voto = '${personaPreSalvataggio.voto}' WHERE id = ${req.params.idLocale}`

        let risultatoUpdate = await pool.query(queryUpdate)

        if (risultatoUpdate.rowCount > 0) {
            res.json({ status: "modificato con successo!" })
        }
        else {
            res.json({ status: "errore, modifica non effettuata" })
        }
    } catch (error) {
        console.log(error)
    }
})

app.post("/auth/login", async (req, res) => {

})

/**
 * Sviluppare una semplice REST API che si connette al DB di tipo PostgreSQL ed effettua delle CRUD di pietanze:
 * Una pietanza è caratterizzata da:
 * - Nome
 * - Descrizione
 * - Prezzo
 * - Senza glutine? (true/false)
 */