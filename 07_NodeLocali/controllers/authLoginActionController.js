const User = require("../models/User")
const bcrypt = require('bcrypt')

module.exports = (req, res) => {

    let utente = {
        username: req.body.inputUser,
        password: req.body.inputPassword
    }

    User.findOne(
        {
            username: utente.username
        }, (err, userResult) => {
            if (!err && userResult) {

                bcrypt.compare(utente.password, userResult.password, (err, same) => {
                    if (same) {
                        req.session.userId = userResult._id
                        req.session.admin = userResult.admin
                        res.status(200).send(userResult)
                    }
                    else {
                        res.status(400).send({ 'error': 'Password non corretta' })
                    }
                })

            }
            else {
                console.log(err)
                res.status(400).send({ 'error': 'Username non corretta' })
            }
        }
    )

}