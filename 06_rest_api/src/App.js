import './App.css';
import React from 'react';
import { Col, Container, Row } from 'react-bootstrap'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import HomeComponent from './components/HomeComponent';
import Navbar from './components/layouts/Navbar';
import AboutComponent from './components/AboutComponent';
import ContactComponent from './components/ContactComponent';
import HookComponent from './components/HookComponent';
import HookComplessoComponent from './components/HookComplessoComponents';
import FromComponent from './components/FormComponent';

function App() {
  return (
    <Container>
      <Row>
        <Col md={6}>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem, itaque magni. Molestiae, soluta itaque placeat eos impedit reiciendis, vero, debitis odit accusamus ad rerum. Soluta delectus reiciendis incidunt nulla provident?</Col>
        <Col md={6}>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem, itaque magni. Molestiae, soluta itaque placeat eos impedit reiciendis, vero, debitis odit accusamus ad rerum. Soluta delectus reiciendis incidunt nulla provident?</Col>
      </Row>
      <Row>
        <FromComponent />
      </Row>
    </Container>
    /* <React.Fragment>
      <HookComponent contatore={0} />
      <HookComplessoComponent />
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<HomeComponent />}></Route>
          <Route path="/about" element={<AboutComponent />}></Route>
          <Route path="/contactUs" element={<ContactComponent />}></Route>
        </Routes>
      </Router>

    </React.Fragment> */
  );
}

export default App;
