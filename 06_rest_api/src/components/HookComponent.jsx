import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';

function HookComponent(props) {

    const [contatore, setContatore] = useState(props.contatore)

    const aggiornaContatore = () => {
        setContatore((prevVal) => (prevVal + 1))
    }

    return (
        <div className="text-center mt-5">
            <h1>Component Hook</h1>
            <h1>{contatore}</h1>

            <button type="button" onClick={aggiornaContatore}>Incrementa</button>
        </div>
    )
}

export default HookComponent;