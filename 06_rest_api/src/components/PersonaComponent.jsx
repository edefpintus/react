import React, { Component } from 'react'

export class PersonaComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            nome: this.props.persona.name,
            email: this.props.persona.email
        }
    }


    render() {
        return (
            <li>
                {this.state.nome} -- {this.state.email}
            </li>
        )
    }
}

export default PersonaComponent
