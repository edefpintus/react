import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';
import { Form, Button } from 'react-bootstrap'

function FromComponent(props) {
    return (
        <React.Fragment>
            <Form>
                <Form.Group className="mt-5">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mt-2">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
            </Form>
        </React.Fragment>
    )

}

export default FromComponent;