import React, { Component } from 'react'
import axios from 'axios'
import PersonaComponent from './PersonaComponent'

export class ListComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            elencoDipendenti: []

        }
    }

    componentDidMount() {
        axios.get('https://gorest.co.in/public/v1/users').then((risultato) => {
            console.log(risultato);
            this.setState((stato) => ({
                elencoDipendenti: risultato.data.data
            }))
        }).catch(() => {
            console.log('Disconesso')
        })
    }

    render() {
        return (
            <React.Fragment>
                <ul>
                    {this.state.elencoDipendenti.map((dipendente, index) => (<PersonaComponent key={index} persona={dipendente} />))}
                </ul>
            </React.Fragment>
        )
    }
}

export default ListComponent
