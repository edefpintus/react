import React, { Component } from 'react'
import ListComponent from './ListComponent';

export class HomeComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
        console.log("Costruita Home");
    }

    componentDidMount() {
        console.log("Home montata");
    }

    componentDidUpdate() {
        console.log("Home Updated");
    }
    componentWillUnmount() {
        console.log("Home unmounted");
    }


    render() {
        return (
            <div>
                <h1>Home Page</h1>
                <ListComponent />
            </div>
        )
    }
}

export default HomeComponent
