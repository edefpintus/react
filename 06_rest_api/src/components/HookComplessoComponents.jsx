import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';

function HookComplessoComponent(props) {

    const [articolo, setArticolo] = useState(
        {
            titolo: 'Titolo della notizia',
            testo: 'Testo della notizia'
        }
    )

    const [persona, setPersona] = useState(
        {
            nome: 'Federico',
            cognome: 'Pintus'
        }
    )

    useEffect(() => {
        console.log('use effect articolo')
    }, [articolo])

    useEffect(() => {
        console.log('use effect persona')
    }, [persona])

    const aggiornaTitolo = () => {
        setArticolo((prev) => ({
            ...prev,
            titolo: 'Nuovo Titolo'
        }))
    }

    const aggiornaPersona = () => {
        setPersona((prev) => ({
            ...prev,
            nome: 'Mario'
        }))
    }

    return (
        <div className="text-center mt-5">
            <h1>{articolo.titolo}</h1>
            <p>{articolo.testo}</p>
            <button type="button" onClick={aggiornaTitolo}>Aggiorna titolo</button>
            <button type="button" onClick={aggiornaPersona}>Aggiorna Persona</button>
        </div>
    )


}
export default HookComplessoComponent