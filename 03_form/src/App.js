import logo from './logo.svg';
import './App.css';
import FormIscrizioneComponent from './components/FormIscrizioneComponent';
import ClockComponent from './components/ClockComponent';

function App() {
  return (
    <div className="container">
      <div className="row ">
        <div className="col text-center">
          <ClockComponent />
        </div>
      </div>
      <FormIscrizioneComponent />
    </div>
  );
}

export default App;
