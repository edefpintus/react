import React, { Component } from 'react'
import TableComponent from './TableComponent'

export class FormIscrizioneComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            val: []
        }
    }

    invioForm = (evt) => {
        evt.preventDefault()
        let prova = {
            nome: evt.target.campoNome.value,
            cognome: evt.target.campoCognome.value,
            note: evt.target.note.value,
            genere: evt.target.inlineRadioOptions.value
        };
        let elencoProva = this.state.val;
        elencoProva.push(prova);
        this.setState((state) => ({
            val: elencoProva
        }));

    }

    render() {
        return (
            <div>
                <h1>Sono il form</h1>

                <form onSubmit={this.invioForm}>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoNome">Nome</label>
                                <input type="text" name="inputNome" id="campoNome" placeholder="Inserisci il valore..." className="form-control" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoCognome">Cognome</label>
                                <input type="text" name="inputCognome" id="campoCognome" placeholder="Inserisci il valore..." className="form-control" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="exampleFormControlTextarea1">Example textarea</label>
                                <textarea className="form-control" id="note" rows="3"></textarea>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="male" value="male" />
                                <label className="form-check-label" htmlFor="inlineRadio1">Male</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="female" value="female" />
                                <label className="form-check-label" htmlFor="inlineRadio2">Female</label>
                            </div>
                            <div className="form-check form-check-inline">
                                <input className="form-check-input" type="radio" name="inlineRadioOptions" id="other" value="other" />
                                <label className="form-check-label" htmlFor="inlineRadio3">Other</label>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <button className="btn btn-primary btn-block">Inserisci</button>
                        </div>
                        <div className="col-md-4"></div>
                    </div>

                </form>

                <hr />

                <h1 className="mt-4">Dettaglio del profilo appena inserito</h1>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Note</th>
                            <th>Genere</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.val.map((obj, idx) => <TableComponent key={idx} prova={obj} />)}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default FormIscrizioneComponent
