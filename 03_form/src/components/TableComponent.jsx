import React, { Component } from 'react'

export class TableComponent extends Component {
    render() {
        const { prova } = this.props
        return (
            < React.Fragment >
                <tr>
                    <td>{prova.nome}</td>
                    <td>{prova.cognome}</td>
                    <td>{prova.note}</td>
                    <td>{prova.genere}</td>
                </tr>
            </React.Fragment >
        )
    }
}

export default TableComponent
