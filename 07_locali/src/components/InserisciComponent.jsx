import axios from 'axios'
import React, { Component } from 'react'

export class InserisciComponent extends Component {

    urlInserimento = "http://127.0.0.1:4000/locali/insert"

    constructor(props) {
        super(props)

        this.state = {
            showTuttoOk: false
        }
    }

    inserisciLocale = (evt) => {
        evt.preventDefault()

        let localeTemp = {
            nome: evt.target.inputNome.value,
            descrizione: evt.target.inputDescrizione.value,
            citta: evt.target.inputCitta.value,
            provincia: evt.target.inputProvincia.value,
            regione: evt.target.inputRegione.value,
            lat: evt.target.inputLat.value,
            long: evt.target.inputLong.value,
            voto: 0
        }

        axios.post(this.urlInserimento, localeTemp).then((risultato) => {

            if (risultato.data.status && risultato.data.status === 'success') {
                this.setState((stato) => ({
                    showTuttoOk: true
                }))

                setTimeout(() => {
                    document.location.href = "/"
                }, 2000);
            }
        })

        console.log(localeTemp)
    }

    render() {

        const tuttoOk = (
            <div className="alert alert-success" role="alert">
                Locale inserito con successo!
            </div>
        )

        const pulsanteInserisci = (
            <React.Fragment>
                <button className="btn btn-outline-success btn-block">Inserisci</button>
            </React.Fragment>
        )

        return (
            <div>
                <h1>SONO INSERISCI</h1>

                <form onSubmit={this.inserisciLocale}>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoNome">Nome</label>
                                <input type="text" className="form-control" name="inputNome" id="campoNome" />
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoDescrizione">Descrizione</label>
                                <input type="text" className="form-control" name="inputDescrizione" id="campoDescrizione" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoCitta">Citta</label>
                                <input type="text" className="form-control" name="inputCitta" id="campoCitta" />
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoProvincia">Provincia</label>
                                <input type="text" className="form-control" name="inputProvincia" id="campoProvincia" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="form-group">
                                <label htmlFor="campoRegione">Regione</label>
                                <input type="text" className="form-control" name="inputRegione" id="campoRegione" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoLong">Long</label>
                                <input type="number" className="form-control" name="inputLong" id="campoLong" />
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="campoLat">Lat</label>
                                <input type="number" className="form-control" name="inputLat" id="campoLat" />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-5">
                        {this.state.showTuttoOk ? tuttoOk : pulsanteInserisci}
                    </div>

                </form>
            </div >
        )
    }
}

export default InserisciComponent

/**
 *  Creare un sistema completo di:
 *  DB - BE - FE
 *
 *  Creare un sistema di gestione locali,
 *  ogni locale è caratterizzato da:
 *  - Nome
 *  - Descrizione
 *  - Città
 *  - Provincia
 *  - Regione
 *  - Latitudine
 *  - Longitudine
 *  - Voto              <----------------------------- da 1 a 5
 *
 *  LATO ADMIN, tutte le path iniziano per /admin/...
 *  1. CRUD locali
 *  1a. L'amministratore vede in diretta le votazioni!
 *  2. CHALLENGE - RESET VOTO LOCALE (fatelo alla fine!)
 *
 *  LATO PUBBLICO
 *  1. Elenco dei locali visualizzabile sotto forma di tabella o lista (come volete, sorprendetemi!)
 *  2. Possibilità di votare un locale, possibilmente una volta sola (utilizzate le tecnologie che conoscete)
 *  3. CHALLENGE - Possibilità di filtrare tra i locali per Città/Provincia/Regione
 */

/**
 * STEP:
 * 1. Sviluppo il DB
 * 2. Metto qualche dato nel DB
 * 3. Sviluppo BE
 * 4. Test del BE con Postman
 * 5. Sviluppo del FE
 */