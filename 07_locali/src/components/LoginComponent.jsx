import React, { Component } from 'react'
import axios from 'axios'

export class LoginComponent extends Component {

    urlRiferimento = "http://localhost:4000/auth/login"

    constructor(props) {
        super(props)

        this.state = {
            loggato: null

        }
    }

    loggati = (evt) => {
        evt.preventDefault()

        let user = {
            username: evt.target.inputUser.value,
            password: evt.target.inputPassword.value

        }

        axios.post(this.urlRiferimento, null, user).then((risultato) => {
            console.log(risultato)
        })

    }


    render() {
        return (
            <React.Fragment>
                <header className="masthead-new">
                    <div className="container position-relative px-4 px-lg-5">
                        <div className="row gx-4 gx-lg-5 justify-content-center">
                            <div className="col-md-10 col-lg-8 col-xl-7">
                                <div className="post-heading">
                                    <h1>LogIn</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <article className="mb-4 mt-4">
                    <div className="container px-4 px-lg-5">
                        <div className="row gx-4 gx-lg-5 justify-content-center">
                            <div className="col-md-10 col-lg-8 col-xl-7">
                                <form onSubmit={this.loggati}>
                                    <div className="form-floating">
                                        <label htmlFor="user">Username</label>
                                        <input className="form-control" name="inputUser" id="user" type="text"
                                            placeholder="Enter your username..." />
                                    </div>
                                    <div className="form-floating">
                                        <label className="mt-3" htmlFor="password">Password</label>
                                        <input className="form-control" name="inputPassword" id="password" type="password"
                                            placeholder="Enter your username..." />
                                    </div>
                                    <button className="btn btn-primary text-uppercase mt-2">Log In</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>
            </React.Fragment>
        )
    }
}

export default LoginComponent



