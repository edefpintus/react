import React, { Component } from 'react'
import RigaComponent from './RigaComponent'
import axios from 'axios'

export class ElencoComponent extends Component {

    urlRiferimento = "http://localhost:4000/locali/list"

    constructor(props) {
        super(props)

        this.state = {
            elencoLocali: [],
            filtrati: [],
            showErrore: false
        }
    }

    contains = (container, string) => {
        return container.indexOf(string) >= 0;
    }

    ricerca = (evt) => {
        evt.preventDefault();
        let searchValue = evt.target.value.toUpperCase();
        const elencoLocaliFiltrato = this.state.elencoLocali.filter((locale) => {
            return (this.contains(locale.nome.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.descrizione.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.citta.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.provincia.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.regione.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.lat.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.long.toUpperCase(), searchValue.toUpperCase()) || this.contains(locale.voto.toUpperCase(), searchValue.toUpperCase()))
        })
        this.setState((stato) => ({ filtrati: elencoLocaliFiltrato }))
        console.log(searchValue)
        console.log(elencoLocaliFiltrato)
    }

    aggiornaElenco = () => {
        axios.get(this.urlRiferimento)
            .then((risultato) => {
                this.setState((stato) => ({ elencoLocali: risultato.data, filtrati: risultato.data }))
            })
            .catch((errore) => {
                this.setState((stato) => ({ showErrore: true }))
            })
    }

    componentDidMount() {
        console.log("Sono nel componentDidMount")
        this.aggiornaElenco();

        /* this.aggiornamentoAutomatico = setInterval(() => {
            this.aggiornaElenco();
        }, 2000); */
    }

    componentWillUnmount() {
        console.log("Sono nel componentWillUnmount")
        clearInterval(this.aggiornamentoAutomatico)
    }

    clickPulsanteAggiorna = () => {
        this.aggiornaElenco();
    }

    render() {
        let htmlErrore                                              //Variabile NULL
        if (this.state.showErrore) {
            htmlErrore = (
                <div className="alert alert-danger" role="alert" >
                    Errore di connessione al server
                </div>
            )
        }

        return (
            <div>
                <div className="row">
                    <div className="col-md-7">
                        <h1>Elenco locali</h1>
                    </div>
                    <div className="col-md-2">
                        <button type="button" className="btn btn-outline-primary btn-block" onClick={this.clickPulsanteAggiorna}>Aggiorna</button>
                    </div>
                    <div className="col-md-2">
                        <form onKeyUp={this.ricerca} className="form-inline my-2 my-lg-0">
                            <input id="search" className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
                        </form>
                    </div>
                </div>

                {htmlErrore && htmlErrore}

                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Descrizione</th>
                            <th>Città</th>
                            <th>Provincia</th>
                            <th>Regione</th>
                            <th>Lat</th>
                            <th>Long</th>
                            <th>Voto</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.filtrati.map((locale, indice) => (
                            <RigaComponent key={indice} contatto={locale} />
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ElencoComponent
