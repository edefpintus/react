import React, { Component } from 'react'
import { useParams } from 'react-router-dom'
import DettaglioLocaleComponent from './DettaglioLocaleComponent';

export function DettaglioComponent() {

    let { id } = useParams();

    return (
        <React.Fragment>
            <DettaglioLocaleComponent identificatore={id} />
        </React.Fragment>
    )
}

export default DettaglioComponent
