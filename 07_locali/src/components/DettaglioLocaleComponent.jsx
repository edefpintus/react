import axios from 'axios'
import React, { Component } from 'react'

export class DettaglioLocaleComponent extends Component {

    urlDettaglio = "http://127.0.0.1:4000/locali/"

    constructor(props) {
        super(props)

        const { identificatore } = this.props

        this.state = {
            identificatore,
            locale: {
                id: null,
                nome: null,
                descrizione: null,
                citta: null,
                provincia: null,
                regione: null,
                lat: null,
                long: null,
                voto: null
            },
            showModifica: false
        }
    }

    componentDidMount() {
        axios.get(this.urlDettaglio + this.state.identificatore).then(risultato => {
            console.log(risultato)

            let arrayRis = risultato.data
            if (arrayRis.length > 0) {
                this.setState((stato) => ({
                    locale: arrayRis[0]
                }))
            }
            else {
                alert("Errore, non trovato!")
            }

            console.log(this.state)
        })
    }

    eliminaLocale = () => {
        axios.delete(this.urlDettaglio + this.state.identificatore)
            .then(risultato => {
                document.location.href = "/"
            })
            .catch(errore => {
                console.log(errore)
                alert("Attenzione, errore di eliminazione")
            })
    }

    abilitaModifica = () => {
        this.setState((stato) => ({
            showModifica: !stato.showModifica
        }))
    }

    modificaLocale = (evt) => {
        evt.preventDefault();

        let personaTemp = {
            nome: evt.target.inputNome.value,
            descrizione: evt.target.inputDescrizione.value,
            citta: evt.target.inputCitta.value,
            provincia: evt.target.inputProvincia.value,
            regione: evt.target.inputRegione.value,
            lat: evt.target.inputLat.value,
            long: evt.target.inputLong.value,
            voto: this.state.locale.voto
        }

        axios.put(this.urlDettaglio + this.state.locale.id, personaTemp).then(
            risultato => {
                document.location.reload()
            }
        )
            .catch(errore => {
                console.log(errore)
                alert("Errore, modifica non effettuata")
            })
    }

    render() {

        const codiceDettaglio = (
            <div className="card">
                <div className="row mt-1 ">
                    <div className="col-11"></div>
                    <div className="col-1">
                        <button type="button" className="btn btn-outline-danger" onClick={this.eliminaLocale}><i className="far fa-trash-alt"></i></button>
                    </div>
                </div>
                <div className="card-body">
                    <h5 className="card-title">
                        {this.state.locale.nome && this.state.locale.nome}&nbsp;
                        {this.state.locale.descrizione && this.state.locale.descrizione}
                    </h5>
                    <p className="card-text">Voto: {this.state.locale.voto && this.state.locale.voto}</p>

                    <button type="button" className="btn btn-outline-info" onClick={this.abilitaModifica}><i className="fas fa-pen"></i></button>
                </div>
            </div>
        )

        const codiceModifica = (
            <form onSubmit={this.modificaLocale}>
                <div className="form-group">
                    <label htmlFor="campoNome">Nome</label>
                    <input type="text" className="form-control" name="inputNome" id="campoNome" defaultValue={this.state.locale.nome} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoDescrizione">Descrizione</label>
                    <input type="text" className="form-control" name="inputDescrizione" id="campoDescrizione" defaultValue={this.state.locale.descrizione} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoCitta">Citta</label>
                    <input type="text" className="form-control" name="inputCitta" id="campoCitta" defaultValue={this.state.locale.citta} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoProvincia">Provincia</label>
                    <input type="text" className="form-control" name="inputProvincia" id="campoProvincia" defaultValue={this.state.locale.provincia} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoRegione">Regione</label>
                    <input type="text" className="form-control" name="inputRegione" id="campoRegione" defaultValue={this.state.locale.regione} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoLat">Lat</label>
                    <input type="number" className="form-control" name="inputLat" id="campoLat" defaultValue={this.state.locale.lat} />
                </div>
                <div className="form-group">
                    <label htmlFor="campoLong">Long</label>
                    <input type="number" className="form-control" name="inputLong" id="campoLong" defaultValue={this.state.locale.long} />
                </div>

                <button className="btn btn-outline-success btn-block">Effettua la modifica</button>
                <button type="button" className="btn btn-outline-info btn-block mt-2" onClick={this.abilitaModifica}>Annulla</button>
            </form>
        )

        return (

            <React.Fragment>
                {this.state.showModifica ? codiceModifica : codiceDettaglio}

            </React.Fragment>
        )
    }
}

export default DettaglioLocaleComponent
