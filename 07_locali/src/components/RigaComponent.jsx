import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

export class RigaComponent extends Component {

    urlDettaglio = "http://127.0.0.1:4000/locali/"

    constructor(props) {
        super(props)

        const { contatto } = this.props
        const { id, nome, descrizione, citta, provincia, regione, lat, long, voto } = contatto;

        this.state = {
            locale: {
                id: id,
                nome: nome,
                descrizione: descrizione,
                citta: citta,
                provincia: provincia,
                regione: regione,
                lat: lat,
                long: long,
                voto: voto
            }
        }
    }

    votaLocale = (evt) => {
        console.log(evt)
        evt.preventDefault();
        axios.get(this.urlDettaglio + this.state.locale.id).then(risultato => {
            console.log(risultato)

            let arrayRis = risultato.data
            if (arrayRis.length > 0) {
                arrayRis[0].voto = arrayRis[0].voto ? (Number.parseInt(arrayRis[0].voto) + 1) : 1
                this.setState((stato) => ({
                    locale: arrayRis[0],

                }))
                axios.put(this.urlDettaglio + this.state.locale.id, this.state.locale).then(
                    result => {
                        console.log("result", result.data)
                    }
                )
                    .catch(errore => {
                        console.log(errore)
                        alert("Errore, voto non salvato")
                    })
            }
            else {
                alert("Errore, non trovato!")
            }

            console.log(this.state)
        })
    }


    render() {
        const urlPersona = "/dettaglio/" + this.state.locale.id
        return (
            <tr>
                <td>{this.state.locale.nome}</td>
                <td>{this.state.locale.descrizione}</td>
                <td>{this.state.locale.citta}</td>
                <td>{this.state.locale.provincia}</td>
                <td>{this.state.locale.regione}</td>
                <td>{this.state.locale.lat}</td>
                <td>{this.state.locale.long}</td>
                <td>{this.state.locale.voto}</td>
                <td>
                    <Link className="btn btn btn-outline-primary btn-inline-block" to={urlPersona}><i title="Pagina Locale" className="far fa-address-card"></i></Link>
                    <button className="btn btn btn-outline-primary btn-inline-block" type="button" onClick={this.votaLocale}><i title="Vota" className="far fa-thumbs-up"></i></button>
                    {/* <Link  to={ }><i title="Vota" className="far fa-thumbs-up"></i></Link> */}
                    {/* <Link to={urlPersona}>Vota</Link> */}
                </td>
            </tr>
        )
    }
}

export default RigaComponent
