import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom'
import ElencoComponent from './components/ElencoComponent';
import InserisciComponent from './components/InserisciComponent';
import DettaglioComponent from './components/DettaglioComponent';
import LoginComponent from './components/LoginComponent';
import Navbar from './components/layouts/Navbar';

function App() {
  return (

    <Router>
      <Navbar />
      <div className="container mt-5">
        <Routes>
          <Route path="/" element={<ElencoComponent />}></Route>
          <Route path="/inserisci" element={<InserisciComponent />}></Route>
          <Route path="/login" element={<LoginComponent />}></Route>
          <Route path="/dettaglio/:id" element={<DettaglioComponent />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
