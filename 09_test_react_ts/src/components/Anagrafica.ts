import { Indirizzo } from './Indirizzo'

export interface Anagrafica {
    nome: string,
    cognome: string,
    indirizzo: Indirizzo
}