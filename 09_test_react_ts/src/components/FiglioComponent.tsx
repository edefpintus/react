import React, { useRef } from 'react'

interface Props {
    effettuaCambiamento: (evt: React.ChangeEvent<HTMLInputElement>) => void
}

export const FiglioComponent = (props: Props) => {

    const refNome = useRef<HTMLInputElement>(null)

    console.log(refNome)
    return (
        <div>
            <h3>Figlio</h3>
            <input ref={refNome} name='inputNome' id='campoNome' type='text' placeholder='Inserisci valore' onChange={props.effettuaCambiamento}></input>
        </div>
    )
}
