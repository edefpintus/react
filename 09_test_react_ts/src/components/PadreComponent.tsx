import React from 'react'
import { FiglioComponent } from './FiglioComponent'
import { Row } from 'react-bootstrap'

interface Props {

}

export const PadreComponent = (props: Props) => {
    return (
        <React.Fragment>
            <Row>
                <FiglioComponent effettuaCambiamento={
                    (evento) => {
                        console.log(evento.target.value)
                    }
                } />
            </Row>
        </React.Fragment>
    )
}
