import React, { useState } from 'react'
import { Anagrafica } from './Anagrafica'

interface Props {
    anagrafica: Anagrafica
}

export const StudenteComponent = (props: Props) => {

    const [matricola, setMatricola] = useState<number>();

    const generaMatricola = () => {
        let numberRandom: number = Math.floor(Math.random() * 1000);
        setMatricola(numberRandom);
    }
    return (
        <div>
            <h1>{props.anagrafica.nome} {props.anagrafica.cognome}</h1>
            <ul>
                <li>Via : {props.anagrafica.indirizzo.via}</li>
                <li>CAP : {props.anagrafica.indirizzo.cap}</li>
                <li>Città : {props.anagrafica.indirizzo.citta}</li>
            </ul>
            {(matricola && matricola > 0) ? (<strong>Matricola: {matricola}</strong>) : (<button onClick={generaMatricola}>Genera Matricola</button>)}
        </div>
    )
}
