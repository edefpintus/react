import React from "react";

interface Props {
    nome: string,
    cognome?: string | undefined | null

}

export const PersonaComponent = (props: Props) => {

    return (
        <div>
            Nome: {props.nome}, Cognome: {props.cognome}
        </div>
    )
}
