import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import { PadreComponent } from './components/PadreComponent';
import { Container } from 'react-bootstrap';

function App() {

  return (
    <React.Fragment>
      <Container>
        <PadreComponent />
      </Container>
    </React.Fragment>
  );
}

export default App;
