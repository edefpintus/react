import React, { Component } from 'react'
import TableComponent from './TableComponent'

export class FiglioVisualComponent extends Component {
    render() {
        const { elenco } = this.props
        return (
            <React.Fragment>
                <h1 className="mt-4">Dettaglio del libro appena inserito</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Titolo</th>
                            <th>ISBN</th>
                        </tr>
                    </thead>
                    <tbody>
                        {elenco.map((obj, idx) => <TableComponent key={idx} elenco={obj} />)}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default FiglioVisualComponent
