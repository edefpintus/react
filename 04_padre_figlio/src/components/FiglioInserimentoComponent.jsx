import React, { Component } from 'react'

export class FiglioInserimentoComponent extends Component {


    invioForm = (evt) => {
        evt.preventDefault()
        let prova = {
            nome: evt.target.campoNome.value,
            cognome: evt.target.campoCognome.value
        };
        this.props.visualizza(prova)

    }
    render() {
        return (
            <div>
                <h1>Sono il form</h1>

                <form onSubmit={this.invioForm}>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoNome">Titolo</label>
                                <input type="text" name="inputNome" id="campoNome" placeholder="Inserisci il valore..." className="form-control" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="campoCognome">ISNB</label>
                                <input type="number" name="inputCognome" id="campoCognome" placeholder="Inserisci il valore..." className="form-control" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            <button className="btn btn-primary btn-block">Inserisci</button>
                        </div>
                        <div className="col-md-4"></div>
                    </div>

                </form>
            </div>
        )
    }
}

export default FiglioInserimentoComponent
