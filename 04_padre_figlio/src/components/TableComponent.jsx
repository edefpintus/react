import React, { Component } from 'react'

export class TableComponent extends Component {
    render() {
        const { elenco } = this.props
        return (
            < React.Fragment >
                <tr>
                    <td>{elenco.nome}</td>
                    <td>{elenco.cognome}</td>
                </tr>
            </React.Fragment >
        )
    }
}

export default TableComponent
