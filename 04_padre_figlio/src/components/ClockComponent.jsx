import React, { Component } from 'react'

export class ClockComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            time: new Date().toLocaleTimeString()
        }
    }
    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.intervalID);
    }
    tick() {
        this.setState({
            time: new Date().toLocaleTimeString()
        });
    }

    render() {
        return (
            <React.Fragment>
                {this.state.time}
            </React.Fragment>
        )
    }
}

export default ClockComponent
