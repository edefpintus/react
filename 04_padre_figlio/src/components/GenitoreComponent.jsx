import React, { Component } from 'react'
import FiglioInserimentoComponent from './FiglioInserimentoComponent'
import FiglioVisualComponent from './FiglioVisualComponent'

export class GenitoreComponent extends Component {
    constructor(props) {
        super(props)

        let personeAttuali = localStorage.getItem("persone") ? JSON.parse(localStorage.getItem("persone")) : []

        this.state = {
            val: personeAttuali

        }
    }

    aggiunta = (prova) => {
        let elencoProva = this.state.val
        elencoProva.push(prova)
        this.setState((state) => ({
            val: elencoProva
        }));

        localStorage.setItem("persone", JSON.stringify(this.state.val))
    }
    render() {
        return (
            <div>
                <FiglioInserimentoComponent visualizza={this.aggiunta} />
                <FiglioVisualComponent elenco={this.state.val} />
            </div>
        )
    }
}

export default GenitoreComponent
