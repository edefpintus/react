import './App.css';
import ClockComponent from './components/ClockComponent';
import GenitoreComponent from './components/GenitoreComponent';

function App() {
  return (
    <div className="container">
      <div className="row ">
        <div className="col text-center">
          <ClockComponent />
        </div>
      </div>
      <GenitoreComponent />
    </div>
  );
}

export default App;
