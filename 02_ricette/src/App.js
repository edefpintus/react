import logo from './logo.svg';
import './App.css';
import ClockComponent from './components/ClockComponent';
import SquadComponent from './components/SquadComponent';

function App() {
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-3">
          <ClockComponent />
        </div>
      </div>
      <div className="row justify-content-center">
        <div className="col-10">
          <SquadComponent name1="GSW" name2="NYK" />
          <SquadComponent name1="LAL" name2="MIA" />
        </div>
      </div>
    </div>
  );
}

export default App;
