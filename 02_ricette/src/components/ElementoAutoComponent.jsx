import React, { Component } from "react";

export class ElementoAutoComponent extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        console.log(this.props)

        const { auto } = this.props

        return (
            <div>
                <h1>{auto}</h1>
                <hr />
            </div>
        )
    }
}

export default ElementoAutoComponent;