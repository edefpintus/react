import React from "react";
import PropType from 'prop-types'

function PassaggioParamentri(props) {
    return (
        <div>
            <h1>Ciao sono {props.name} {props.surname}, ho {props.age} anni</h1>
        </div>
    )
}

PassaggioParamentri.PropType = {
    name: PropType.string,
    surname: PropType.string,
    age: PropType.number
}

export default PassaggioParamentri;