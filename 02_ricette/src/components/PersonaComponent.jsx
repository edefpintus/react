import { Component } from "react";

export class PersonaComponent extends Component {

    constructor(props) {
        super(props)

        this.name = this.props.name ? this.props.name.toUpperCase() : '';
        this.sur = this.props.sur ? this.props.sur.toUpperCase() : '';
    }

    render() {

        return (
            <div>
                <h1>Nome: {this.name}</h1>
                <h2>Cognome: {this.sur}</h2>
            </div>
        )
    }
}