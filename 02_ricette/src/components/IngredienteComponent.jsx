import React, { Component } from 'react'

export class IngredienteComponent extends Component {
    render() {

        const { prod, quant, unit } = this.props;
        return (
            <React.Fragment>
                <tr>
                    <td>{prod}</td>
                    <td>{quant}</td>
                    <td>{unit}</td>
                </tr>
            </React.Fragment>
        )
    }
}

export default IngredienteComponent
