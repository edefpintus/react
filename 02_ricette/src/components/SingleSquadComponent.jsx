import React, { Component } from 'react'

export class SingleSquadComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            punteggio: 0
        }
    }

    plus = () => {
        this.setState((state) => ({ punteggio: Number.parseInt(state.punteggio) + 1 }))
    }

    minus = () => {
        this.setState((state) => ({ punteggio: (this.state.punteggio <= 0 ? 0 : Number.parseInt(state.punteggio) - 1) }))
    }

    render() {
        return (
            <React.Fragment>
                <td>
                    <button className="btn btn-secondary" onClick={this.minus}>-</button>
                    <label>{this.state.punteggio}</label>
                    <button className="btn btn-light" onClick={this.plus}>+</button>
                </td>
            </React.Fragment>
        )
    }
}

export default SingleSquadComponent
