import React, { Component } from 'react'
import IngredienteComponent from './IngredienteComponent'

export class RicettaComponent extends Component {
    constructor(props) {
        super(props)
    }

    render() {

        const { ricettaNome, ingredienti } = this.props

        return (
            <div>
                <h1>{ricettaNome}</h1>
                <table className="table" style={{ color: 'white' }}>
                    <tbody>
                        {ingredienti.map((obj, idx) => <IngredienteComponent key={idx} prod={obj.prod} quant={obj.quant} unit={obj.unit} />)}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default RicettaComponent
