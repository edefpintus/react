import React, { Component } from 'react'
import RicettaComponent from './RicettaComponent'

export class StampaRicetta extends Component {

    constructor(props) {
        super(props)

        this.ricette = [
            {
                nome: 'Carbonara',
                descrizione: "Molto buona",
                ricetta: [
                    {
                        prod: 'pasta',
                        quant: 1,
                        unit: 'kg'
                    },
                    {
                        prod: 'uova',
                        quant: 2,
                        unit: 'u'
                    },
                    {
                        prod: 'guanciale',
                        quant: 300,
                        unit: 'g'
                    }
                ]
            },
            {
                nome: 'Pasta e tonno',
                descrizione: "Molto buona",
                ricetta: [
                    {
                        prod: 'pasta',
                        quant: 1,
                        unit: 'kg'
                    },
                    {
                        prod: 'tonno',
                        quant: 700,
                        unit: 'g'
                    }
                ]
            }
        ]
    }


    render() {

        const elencoJsx = (
            <React.Fragment>
                {this.ricette.map((obj, idx) => <RicettaComponent key={idx} ricettaNome={obj.nome} ingredienti={obj.ricetta} />)}
            </React.Fragment>
        )

        return (
            <React.Fragment>
                {elencoJsx}
            </React.Fragment>
        )
    }
}

export default StampaRicetta
