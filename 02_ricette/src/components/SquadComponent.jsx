import React, { Component } from 'react'
import SingleSquadComponent from './SingleSquadComponent'

export class SquadComponent extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { name1, name2 } = this.props;
        return (
            <React.Fragment>
                <table className="table">
                    <thead>
                        <tr>
                            <th>{name1}</th>
                            <th>{name2}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <SingleSquadComponent />
                            <SingleSquadComponent />
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default SquadComponent
