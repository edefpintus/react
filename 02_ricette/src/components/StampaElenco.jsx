import React, { Component } from 'react'
import ElementoAutoComponent from './ElementoAutoComponent'

export class StampaElenco extends Component {

    constructor(props) {
        super(props)

        this.automobili = ["BMW", "Lambo", "FIAT", "Maserati"]
    }

    render() {

        const elencoJsx = (
            <React.Fragment>
                {this.automobili.map((obj, idx) => <ElementoAutoComponent key={idx} auto={obj} />)}
            </React.Fragment>
        )

        return (
            <React.Fragment>
                {elencoJsx}
            </React.Fragment>
        )
    }
}
