import React, { Component } from "react";

export class ParamClass extends Component {

    utente;
    constructor(props) {
        super(props)

        this.utente = new Object();
        this.utente.name = this.props.name ? this.props.name : `N.D.`;
        this.utente.surname = this.props.surname ? this.props.surname : `N.D.`;
        this.utente.age = this.props.age ? `${this.props.age} anni` : `N.D.`;
    }
    render() {
        return (
            <React.Fragment>
                <td>{this.utente.name}</td>
                <td>{this.utente.surname}</td>
                <td>{this.utente.age}</td>
            </React.Fragment>
        )

    }
}