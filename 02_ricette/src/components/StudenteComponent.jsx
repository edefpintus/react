import { PersonaComponent } from "./PersonaComponent";

export class StudenteComponent extends PersonaComponent {

    constructor(props) {
        super(props)

        this.name = this.props.name ? this.props.name.toUpperCase() : '';
        this.sur = this.props.sur ? this.props.sur.toUpperCase() : '';
        this.matr = this.props.matr ? this.props.matr.toUpperCase() : '';
    }

    render() {

        return (
            <div>
                <h1>Nome: {this.name}</h1>
                <h2>Cognome: {this.sur}</h2>
                <h2>Matricola: {this.matr}</h2>
            </div>
        )
    }
}