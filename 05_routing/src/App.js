import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import HomeComponent from './components/HomeComponent';
import Navbar from './components/layouts/Navbar';
import AboutComponent from './components/AboutComponent';
import ContactComponent from './components/ContactComponent';

function App() {
  return (
    <React.Fragment>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<HomeComponent />}></Route>
          <Route path="/about" element={<AboutComponent />}></Route>
          <Route path="/contactUs" element={<ContactComponent />}></Route>
        </Routes>
      </Router>

    </React.Fragment>
  );
}

export default App;
