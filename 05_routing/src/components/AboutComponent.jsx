import React, { Component } from 'react'

export class AboutComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
        console.log("Costruita About");
    }

    componentDidMount() {
        console.log("About montata");
    }

    componentDidUpdate() {
        console.log("About Updated");
    }
    componentWillUnmount() {
        console.log("About unmounted");
    }


    render() {
        return (
            <div>
                <h1>About Page</h1>
            </div>
        )
    }
}

export default AboutComponent
