import React, { Component } from 'react'

export class ContactComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
        console.log("Costruita Contact");
    }

    componentDidMount() {
        console.log("Contact montata");
    }

    componentDidUpdate() {
        console.log("Contact Updated");
    }
    componentWillUnmount() {
        console.log("Contact unmounted");
    }


    render() {
        return (
            <div>
                <h1>Contact Page</h1>
            </div>
        )
    }
}

export default ContactComponent
