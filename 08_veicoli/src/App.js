import './App.css';
import { Col, Container, Row } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import FormComponent from './components/FormComponent';
import React from 'react';


function App() {
  return (
    <Container>
      <FormComponent />
    </Container>
  );
}

export default App;



/**
 * Creare una SPA che permetta l'inserimento di un solo componente di:
 * Dettaglio del proprietario
 * -- Nome, Cognome, CF, Indirizzo (campo composito)
 * Dettaglio dei veicoli posseduti
 * -- Per ogni veicolo: Targa, Data di acquisto, Modello, Marca
 *
 * Trovare una soluzione per salvare i dati tramite Hooks all'interno del LocalStorage (se volte con REST API)
 * E che utilizzi react-boostrap come lib grafica. Dove potete non utilizzate i className ma le componenti di rb
 */
