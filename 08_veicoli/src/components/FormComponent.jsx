import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';
import { Form, Button, Col, Row } from 'react-bootstrap'

function FormComponent(props) {

    const [persona, setPersona] = useState(
        {
            nome: 'N.D',
            cognome: 'N.D',
            CF: 'N.D',
            Address: 'N.D',
            ZipCode: 'N.D',
            State: 'N.D'

        }
    )

    useEffect(() => {
        console.log('use effect persona')
    }, [persona])

    const invioForm = (evt) => {
        evt.preventDefault();
        console.log('prova', evt)
        /* setPersona(() => ({
            nome: 'N.D',
            cognome: 'N.D',
            CF: 'N.D',
            Address: 'N.D',
            ZipCode: 'N.D',
            State: 'N.D'
        })) */
    }

    return (
        <React.Fragment>
            <Form onSubmit={invioForm}>
                <Row>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>Name</Form.Label>
                            <Form.Control id='name' name='name' type="text" placeholder="Enter Name" />
                            {/* <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text> */}
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>Surname</Form.Label>
                            <Form.Control type="text" placeholder="Enter Surname" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>CF</Form.Label>
                            <Form.Control type="text" placeholder="Enter CF" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" placeholder="Enter Address" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>ZipCode</Form.Label>
                            <Form.Control type="text" placeholder="Enter ZipCode" />
                        </Form.Group>
                    </Col>
                    <Col md={6}>
                        <Form.Group className="mt-5">
                            <Form.Label>State</Form.Label>
                            <Form.Control type="text" placeholder="Enter State" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col className="mt-5">
                        <Button type='submit' variant="outline-primary">Inserisci il valore</Button>
                    </Col>
                </Row>
            </Form>
        </React.Fragment>
    )

}

export default FormComponent;